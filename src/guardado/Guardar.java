package guardado;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Guardar {
    private int puntos;
    private String nombreJugador;
    public ArrayList<String>recuperar;
    
    public Guardar(int puntos, String nombreJugador){
        this.puntos = puntos;
        this.nombreJugador = nombreJugador;
        this.leer();
        this.Escribir();
    }
    
    public void Escribir()
    {
        //Agregar manejo de archivo para que guarde el nombre del jugador y el puntaje.
        File f;
        FileWriter w;
        BufferedWriter bw;
        PrintWriter wr;
        
        try{
            f=new File("Puntaje.txt");//crea archivo
            w=new FileWriter(f);
            bw=new BufferedWriter(w);
            wr=new PrintWriter(bw);
            
           // wr.write("PUNTAJES: \n");
            
            /*----------------------------------------------------------------------*/
            wr.append(this.nombreJugador+" "+this.puntos+"\n");
            for(int i=0;i<this.recuperar.size();i++){
                wr.append(recuperar.get(i)+"\n");}
            /*----------------------------------------------------------------------*/
            
            wr.close();
            bw.close();
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,"ha sucedido un error"+e);
            
        }
    }
    
    public void leer(){
        File Archivo;
        FileReader fr;
        BufferedReader br;
        this.recuperar=new ArrayList<String>();
        try{
            Archivo =new File("Puntaje.txt");//busca el archivo
            fr=new FileReader(Archivo);
            br=new BufferedReader(fr);
           
            
            String linea;
            while((linea=br.readLine())!= null){
                //System.out.println(linea);
                recuperar.add(linea);
                
            }
            br.close();
            fr.close();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,"Hubo un error leyendo el Archivo"+e); 
        }
        }
    public ArrayList<String> guardarArray(){
        File Archivo;
        FileReader fr;
        BufferedReader br;
        ArrayList<String>recuperar2=new ArrayList<String>();
        this.recuperar=new ArrayList<String>();
        try{
            Archivo =new File("Puntaje.txt");//busca el archivo
            fr=new FileReader(Archivo);
            br=new BufferedReader(fr);
           
            
            String linea;
            while((linea=br.readLine())!= null){
                //System.out.println(linea);
                recuperar2.add(linea);
                
            }
            br.close();
            fr.close();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,"Hubo un error leyendo el Archivo"+e); 
        }
        return recuperar2;
        }
    public ArrayList<String> Lista(){
    	return recuperar;
    }


    
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Guardar m=new Guardar(1,"h-------ssl");
        m.Escribir();
        Guardar s=new Guardar(2,"gonzalo");
        s.Escribir();
        ArrayList<String>lista=s.guardarArray();
        for(int i=0;i<lista.size();i++){
        	System.out.println(lista.get(i));
        }
        
       
        
        }
        
       

}
